<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDO</title>
</head>
<body>
 <?php

// EXO N°1
try
{
    $user = "root";
    $pwd = "root";
	$bdd = new PDO('mysql:host=localhost;dbname=colyseum;charset=utf8', $user, $pwd, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}
$reponse = $bdd->query('SELECT * FROM clients');

while ($donnees = $reponse->fetch()) {
    ?>
    <p><strong>Clients</strong> :<?php echo $donnees['lastName'].'<br>';
                                       echo $donnees['firstName'].'<br>'; ?> </p>
<?php } 
$reponse->closeCursor();
// EXO N°2
$rep2 = $bdd->query('SELECT * FROM shows');
while ($donnees = $rep2->fetch()) {
    ?>  <p><strong>Shows</strong> :<?php echo $donnees['title'].'<br>';
} 
$rep2->closeCursor();?>
<?php
// EXO N°3
$rep3 = $bdd->query('SELECT * FROM clients LIMIT 0, 20');
echo '<p>Voici les 20 premières entrées de la table clients :</p>';

while ($donnees = $rep3->fetch()) {
    echo $donnees['lastName'].'<br>';
    echo $donnees['firstName'].'<br>';
    echo '<hr>';
}
$rep3->closeCursor();
?> 

<?php
// EXO N°4
$rep3 = $bdd->query('SELECT * FROM clients');
echo '<p>Voici les entrées de la table clients pour les clients ayant une carte de fidélité :</p>';

while ($donnees = $rep3->fetch()) {
    if ($donnees['card'] == true) {
        echo $donnees['lastName'].'<br>';
        echo $donnees['firstName'].'<br>';
        echo '<hr>';
    }
}
$rep3->closeCursor();
?>

<?php
// EXO N°5
// $m = 'M%';
$rep3 = $bdd->query('SELECT * FROM clients WHERE lastName LIKE "M%" ORDER BY firstName');
// var_dump($rep3);
// foreach($rep3 as $row){
//     var_dump($row);
// }

echo '<p>Voici les entrées de la table clients pour les clients ayant un nom et prénom commençant par la lettre M :</p>';

while ($donnees = $rep3->fetch()) {
// if ($rep3 == true) {
    echo "Nom du client: ". $donnees['lastName'].'<br>';
    echo "Prénom du client: ". $donnees['firstName'].'<br>';
    echo '<hr>';
// }
    
}
$rep3->closeCursor();
?>
<?php 
##Exercice 6

// Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure.
//  Trier les titres par ordre alphabétique. 
//  Afficher les résultat comme ceci : *Spectacle* par *artiste*, 
//  le *date* à *heure*.

$resp =$bdd->query('SELECT * FROM shows ORDER BY title');

while($data = $resp->fetch()) {
    echo "Spectacle :". $data['title']." par: ".$data['performer']." le : ".$data['date']." à: ".$data['startTime']."<br>";
}
$resp->closeCursor();
?>

<?php 
##Exercice 7
// Afficher tous les clients comme ceci :
$resp =$bdd->query('SELECT * FROM clients');
while($data = $resp->fetch()) {
    // Nom : *Nom de famille du client*
    echo "Nom: ".$data['lastName']."<br>";
    // Prénom : *Prénom du client*
    echo "Prénom: ".$data['firstName']."<br>";
    // Date de naissance : *Date de naissance du client*
    echo "Date de naissance: ".$data['birthDate']."<br>";
    // Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
    echo "Carte de fidélité: ";
    if ($data['card'] == true) {
        echo "OUI"."<br>";
        // Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*
        echo "Numéro: ".$data['cardNumber']."<br>"."<hr>";
    } else {
        echo "NON"."<br>"."<hr>";
    }
}
$resp->closeCursor();
?>

</body>
</html>